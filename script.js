const images = document.querySelectorAll('.image-to-show');
const startButton = document.querySelector('.play');
const stopButton = document.querySelector('.stop');
const firstImage = images[0];
const lastImage = images[images.length - 1];

const slider = () => {
  const showHiddenImage = document.querySelector('.visible');
  if (showHiddenImage !== lastImage) {
    showHiddenImage.classList.remove('visible');
    showHiddenImage.nextElementSibling.classList.add('visible');
  } else {
    showHiddenImage.classList.remove('visible');
    firstImage.classList.add('visible');
  }
};

let timer = setInterval(slider, 3000);

stopButton.addEventListener('click', () => {
  clearInterval(timer);
  startButton.disabled = false;
  stopButton.disabled = true;
});

startButton.addEventListener('click', () => {
  timer = setInterval(slider, 3000);
  startButton.disabled = true;
  stopButton.disabled = false;
});

